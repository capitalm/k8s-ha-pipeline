#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
from kubernetes import client
import logging
from operator import itemgetter
from os import path
import random
import re
import subprocess
import time

API_V1 = None
SETTINGS = {
    'namespace': 'default',
    'parallel': 2,
}
RE_POD = re.compile(r'(?P<app>.+)-(?P<id>[0-9]+)$')
HEALTHY = {
    'DiskPressure': 'False',
    'OutOfDisk': 'False',
    'MemoryPressure': 'False',
    'Ready': 'True',
    'KernelDeadlock': 'False',
    'NetworkUnavailable': 'False',
}
logger = logging.getLogger(__name__)


def main():
    while True:
        run()
        time.sleep(1)


def run():
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)
    random.seed()
    if not should_run():
        return

    scores = {}
    base_score(scores)
    apps = app_scores(scores)
    for app in apps.keys():
        bind_app(app, apps[app], scores)


def should_run():
    v1 = api_v1()
    ns = SETTINGS['namespace']
    selector = 'spec.nodeName='

    for pod in v1.list_namespaced_pod(namespace=ns,
                                      field_selector=selector).items:
        if pod.spec.scheduler_name == 'ha-pipeline':
            return True
    return False


def base_score(scores):
    v1 = api_v1()
    for node in v1.list_node().items:
        name = node.metadata.name
        scores[name] = {'_base': 0}
        for cond in node.status.conditions:
            try:
                if cond.status != HEALTHY[cond.type]:
                    scores[name]['_base'] += 100
            except KeyError:
                pass


def app_scores(scores):
    v1 = api_v1()
    ns = SETTINGS['namespace']
    parallel = SETTINGS['parallel']
    apps = {}

    for po in v1.list_namespaced_pod(ns).items:
        node = po.spec.node_name
        if po.spec.scheduler_name != 'ha-pipeline':
            if node:
                scores[node]['_base'] += 1
            continue

        name = po.metadata.name
        match = RE_POD.match(name)
        if not match:
            if node:
                scores[node]['_base'] += 1
            continue

        app = match.group('app')
        id_ = int(match.group('id'))
        group = int(id_ / parallel)
        if node:
            add_score(scores[node], app, group)
        else:
            if app not in apps:
                apps[app] = []
            apps[app].append((name, group))
    return apps


def add_score(node, app, group):
    if app not in node:
        node[app] = {'_base': 0}
    if group not in node[app]:
        node[app][group] = 0
    node[app][group] += 10
    node[app]['_base'] += 1


def bind_app(app, pods, scores):
    for pod, group in pods:
        node = min(calc_scores(app, group, scores),
                   key=itemgetter(1))
        bind_pod(node[0], app, group, pod, scores)


def calc_scores(app, group, scores):
    for node in scores:
        if app not in scores[node]:
            scores[node][app] = {'_base': 0}
        score = (random.random() +
                 scores[node]['_base'] +
                 scores[node][app]['_base'] +
                 scores[node][app].get(group, 0))
        yield (node, score)


def bind_pod(node, app, group, pod, scores):
    logger.info('Binding pod %s to node %s', pod, node)
    namespace = SETTINGS['namespace']
    target = client.V1ObjectReference(api_version='v1', kind='Node', name=node)
    body = client.V1Binding(api_version='v1', kind='Binding', target=target,
                            metadata={'name': pod})
    api_v1().create_namespaced_pod_binding(pod, namespace, body)
    add_score(scores[node], app, group)


def api_v1():
    global API_V1
    if API_V1:
        return API_V1

    init_settings()
    host = SETTINGS['host']
    cacert_path = SETTINGS['cacert_path']
    token_path = SETTINGS.get('token_path', None)
    cert_path = SETTINGS.get('cert_path', None)
    key_path = SETTINGS.get('key_path', None)

    client.configuration.host = host
    client.configuration.ssl_ca_cert = cacert_path

    if token_path:
        with open(token_path, 'r') as token_file:
            token = token_file.read()
        client.configuration.api_key['authorization'] = token
        client.configuration.api_key_prefix['authorization'] = 'Bearer'

    if cert_path and key_path:
        client.configuration.cert_file = cert_path
        client.configuration.key_file = key_path

    API_V1 = client.CoreV1Api()
    return API_V1


def init_settings():
    if path.exists('/var/run/secrets/kubernetes.io/'):
        init_inpod()
    elif path.exists(path.expanduser('~/.minikube')):
        init_minikube()
    else:
        raise Exception('Cannot detect kubernetes access method')


def init_inpod():
    global SETTINGS
    SETTINGS.update({
        'token_path': '/var/run/secrets/kubernetes.io/serviceaccount/token',
        'cacert_path': '/var/run/secrets/kubernetes.io/serviceaccount/ca.crt',
        'host': 'https://kubernetes.default.svc.cluster.local',
    })


def init_minikube():
    global SETTINGS
    process = subprocess.Popen(['minikube', 'ip'], stdout=subprocess.PIPE)
    out, _ = process.communicate()
    ip = out.decode('utf-8').strip()

    minikube = path.expanduser('~/.minikube')
    SETTINGS.update({
        'cacert_path': path.join(minikube, 'ca.crt'),
        'cert_path': path.join(minikube, 'apiserver.crt'),
        'key_path': path.join(minikube, 'apiserver.key'),
        'host': 'https://{}:8443'.format(ip),
    })


if __name__ == '__main__':
    main()
