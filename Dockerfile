FROM python:3-alpine

RUN pip --no-cache-dir install \
  kubernetes==3.0.0

ADD ./k8s_ha_pipeline.py /k8s_ha_pipeline.py
RUN chmod +x /k8s_ha_pipeline.py

CMD ["python3", "/k8s_ha_pipeline.py"]
